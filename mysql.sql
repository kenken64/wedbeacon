CREATE DATABASE `wedbeacon` /*!40100 DEFAULT CHARACTER SET utf8 */;

use wedbeacon;

DROP TABLE `events`;
DROP TABLE `attendees`;
DROP TABLE `gallery`;
DROP TABLE `comments`;


CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `beacon` varchar(80) DEFAULT NULL,
  `fullAddress` varchar(255) DEFAULT NULL,
  `groomName` varchar(255) DEFAULT NULL,
  `brideName` varchar(255) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `attendees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `emailAddress` varchar(45) DEFAULT NULL,
  `mobileNo` varchar(45) DEFAULT NULL,
  `attend_datetime` datetime DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_EVENT_ID_idx` (`event_id`),
  CONSTRAINT `FK_EVENT_ID` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) DEFAULT NULL,
  `likeCount` int(11) DEFAULT 0,
  `filename` varchar(45) DEFAULT NULL,
  `updateDateTime` datetime DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`),
  KEY `FK_GALLERY_EVTID_idx` (`event_id`),
  CONSTRAINT `FK_GALLERY_EVTID` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `comments` text,
  `comment_id` int(11) DEFAULT NULL,
  `attendees_id` int(11) DEFAULT NULL,
  `updatedDatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_COMMENTS_GID_idx` (`gallery_id`),
  CONSTRAINT `FK_COMM_GID` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
