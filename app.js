const version = '1.0.0';

'use strict' // enforce strict mode on the javascript coding.

var express = require("express");
var app = express();

var mysql = require("mysql");
var path = require("path");
var bodyParser = require('body-parser');
var fs = require("fs");
var upload = require('multer')();
var extend = require('extend')

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

var configfile = fs.readFileSync("config/config.json");
var jsonConfig = JSON.parse(configfile);

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = jsonConfig.cryptoPassword
;

console.log("db username : "  + jsonConfig.dbusername);
console.log("db hostname : " + jsonConfig.dbHostname);
console.log("db port : " + jsonConfig.dbPort);
console.log("db Connection limit : " + jsonConfig.dbConnLimit);
console.log("db encrypted password : " + jsonConfig.dbpassword);
console.log("db clear password : " + decrypt(jsonConfig.dbpassword));

var pool = mysql.createPool({
    host: jsonConfig.dbHostname,
    port: jsonConfig.dbPort,
    user: jsonConfig.dbusername,
    password: decrypt(jsonConfig.dbpassword),
    database: jsonConfig.dbName,
    connectionLimit: jsonConfig.dbConnLimit
});


const insertComment = "insert into comments (gallery_id, comments, comment_id, attendees_id, updatedDatetime) " +
    "values (?, ?, ?, ?, ?)";
const getAllCommentsByGalleryId = "select id, gallery_id, comments, comment_id, attendees_id, updatedDatetime " +
    "from comments order by updatedDatetime";

const updateComment = "update comments set comments = ?, updatedDatetime = ? where id = ?";

function encrypt(text){
  var cipher = crypto.createCipher(algorithm,password)
  var crypted = cipher.update(text,'utf8','hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text){
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    return dec;
}


app.get('/', function(req, res) {
	 res.redirect("gallery.html");
});

app.get('/gallery', function(req, res) {
	 res.redirect("gallery.html");
});

app.get("/api/events/:event_id", function (req, res) {
	console.log("Get event ...");
	pool.getConnection(function (err, connection) {
        	if (err) {
					console.log(err);
            		res.status(400).send(JSON.stringify(err));
            		return;
        	}
        	var query = connection.query("select * from events where id = ?",
            	[req.params.event_id],
            	function (err, results) {
                	connection.release();
                	if (err) {
						console.log(err);
						res.status(400).send(JSON.stringify(err));
						return;
                	}
                	if (results.length){
						console.log(results[0]);
						res.json(results[0]);
                	}else{
						res.status(404).end("id " + req.params.emp_no + " is not found");
					}
				});
            connection.release();
			console.log(query.sql);
    	});


});

app.get('/api/events', function(req, res) {
    pool.getConnection(function (err, connection) {
        connection.query("select * from events", function(err, results) {
            if (err) {
                  res.status(400).send(JSON.stringify(err));
                  return;
            }
            connection.release()
            res.json(results)
        })
    });
});


app.post('/api/events', function(req, res) {
    var event = {};

    // Copy all necessary fields from the req.params into the event object
    extend( true, event, req.params )

    pool.getConnection(function (err, connection) {
        var query = "insert into events (name, startDate, endDate, latitude, longitude, beacon, fullAddress, groomName, brideName, updatedDateTime)"
        query += " values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        connection.query(query, [
          event.name, event.startDate, event.endDate, event.latitude, event.longitude, event.beacon, event.fullAddress,
          event.groomName, event.brideName, event.updatedDateTime
        ], function(err, result) {
            if (err) {
                  res.status(400).send(JSON.stringify(err));
                  return;
            }
            connection.release()
            res.json(event)
        })
    });
});

app.get("/api/attendees/:attendees_id", function (req, res) {
    pool.getConnection(function (err, connection) {
        connection.query("select * from attendees where id = ?" , [req.params.attendees_id], function(err, results) {
            if (err) {
                res.status(400).send(JSON.stringify(err));
                return;
            }
            connection.release();
            res.json(results[0])
        })
    });
});


app.get('/api/galleries', function(req, res) {
    pool.getConnection(function (err, connection) {
        connection.query("select id, event_id, filename, updateDateTime, likeCount from gallery", function(err, results) {
            if (err) {
                res.status(400).send(JSON.stringify(err));
                return;
            }
            connection.release();
            res.json(results)
        })
    });

});


app.get("/api/gallery/:gallery_id", function (req, res) {
    pool.getConnection(function (err, connection) {
        connection.query("select * from gallery where id = ?", [req.params.gallery_id], function(err, results) {
            if (err) {
                res.status(400).send(JSON.stringify(err));
                return;
            }
            connection.release();
            res.json(results)
        })
    });
});

app.get("/api/gallery/:gallery_id/like", function (req, res) {
    pool.getConnection(function (err, connection) {
        connection.query("UPDATE gallery SET likeCount = likeCount + 1 WHERE id = ?", [req.params.gallery_id], function(err, results) {
            connection.release();
            if (err) {
                return res.status(400).send(JSON.stringify(err));
            }
            res.json(results);
        })
    });
});

app.post('/api/gallery', upload.single('photo'), function(req, res) {
    var photo = {
        "updateDateTime": new Date(),
        "filename": req.file.originalfilename,
        "eventId": req.body.eventId
    }

    photo.eventId = req.body["event_id"]
    photo.data = req.file.buffer
    pool.getConnection(function (err, connection) {
        if (err) {
            res.status(400).send(JSON.stringify(err));
            return;
        }

        connection.query("insert into gallery(event_id, filename, updateDateTime, data) values(?, ? , ?, ?)", [
            photo.eventId,
            photo.filename,
            photo.updateDateTime,
            photo.data
        ], function(err, result) {
            console.log(result)
            connection.release();
            if (err) {
                res.status(400).send(JSON.stringify(err));
                return;
            }

            if (result){
                res.json(photo)
                return;
            } else {
                res.status(404).end("Could not upload to gallery");
            }
        })
    });
});

app.get("/api/render/:gallery_id", function(req, res) {
    pool.getConnection(function(err, connection) {
        connection.query("select data from gallery where id = ?", [req.params.gallery_id], function(err, result) {
            if(err) {
                res.status(404).end("Some error occured");
                return;
            }
            if(result.length) {
                connection.release();
                res.setHeader('Content-Type', 'image/png');
                res.send(result[0].data)
            } else {
                res.status(404).end("Image not found");
            }
        })
    })
});

app.get("/api/comments", function (req, res) {
	console.log("get all comments tied to a photo");
    pool.getConnection(function (err, connection) {
        connection.query(getAllCommentsByGalleryId, function(err, results) {
            if (err) {
                console.log(err);
                res.status(400).send(JSON.stringify(err));
                return;
            }
            connection.release();
            console.log(results);
            res.json(results)
        })
    });
});

app.post('/api/comment', function(req, res) {

	var stringify = JSON.stringify(req.body.params.comments);
	var commentJson = JSON.parse(req.body.params.comments);
    var commentVal = commentJson["comments"];
    var commentSqlParams = [
        commentJson["gallery_id"],
        commentVal,
        commentJson["comment_id"],
        commentJson["attendees_id"],
        new Date()
    ];


	pool.getConnection(function(err, connection){
		if (err) {
			console.log(err);
			res.status(500).end();
			return;
		}
		var query = connection.query(insertComment, commentSqlParams,
		function(err, result){
			connection.release();
			if (err) {
				console.log(err);
				res.status(500).end();
				return;
			}
			console.log(result);
			res.status(202).json({url: "/api/comment/" + result['id']});
		});
		console.log(query.sql);
	});

});

app.put('/api/comment', function(req, res) {

    var stringify = JSON.stringify(req.body.params.comments);
    var commentJson = JSON.parse(req.body.params.comments);
    var commentVal = commentJson["comments"];
    var commentSqlParams = [
        commentVal,
        new Date,
        commentJson["id"]
    ];


    pool.getConnection(function(err, connection){
        if (err) {
            console.log(err);
            res.status(500).end();
            return;
        }
        var query = connection.query(updateComment, commentSqlParams,
            function(err, result){
                connection.release();
                if (err) {
                    console.log(err);
                    res.status(500).end();
                    return;
                }
                console.log(result);
                res.status(202).json({url: "/api/comment/" + result['id']});
            });
        console.log(query.sql);
    });

});


app.use("/bower_components", express.static(path.join(__dirname, "bower_components")));
app.use(express.static(path.join(__dirname, "public")));

app.use(function(req, res, next) {
    res.type("text/plain");
    res.status(404);
    res.end("Page not found");
});

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get("port"), function () {
    console.info("Middleware started on port %d", app.get("port"));
});
