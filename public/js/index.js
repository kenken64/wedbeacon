angular
    .module("wedApp", [])
    .controller("wedAppCtrl", wedAppCtrl);

angular
    .module('wedApp')
    .factory('focus', focus);


function wedAppCtrl($http, focus) {
    var vm = this;
    vm.comments = [];
    vm.attendeesName = "Kenneth Phang";
    vm.comment = createComment();
    vm.comment.gallery_id = 1;
    vm.comment.comment_id = 1;
    vm.comment.attendees_id = 1;
    vm.currentUpdateId = 0;

    $http.get('/api/comments')
        .success(function (data) {
            vm.comments = data;
     });

    vm.addComment = function (imageId) {
        fillComment = fillupComment(vm.comment, imageId)

        if(vm.comment.id > 0){
            console.info(currentUpdateId);
            console.info(imageId);
            console.info(" Update ");
            vm.comments.forEach( function(item){
                if(item.id ==  currentUpdateId){
                    var idxVal = vm.comments.indexOf(item);
                    console.log(idxVal);
                    vm.comments[idxVal] = fillComment;
                }
            });
            $http.put('/api/comment', {
                params: {
                    comments: JSON.stringify(vm.comment),
                    headers: {'Content-Type': 'application/json'}
                }
            })
            .success(function (data) {

            });
        }else {
            console.info(" Insert ");
            vm.comments.push(fillComment);
            $http.post("/api/comment", {
                params: {
                    comments: JSON.stringify(vm.comment),
                    headers: {'Content-Type': 'application/json'}
                }
            }).then(function () {
                //
            }).catch(function () {
                //
            });
        }
        vm.comment = createComment();

    };

    vm.updateComment = function (commentId, comments, index, gallery_id) {
        vm.comment.id = commentId;
        vm.comment.comments = comments;
        console.info(commentId);
        console.info(index);
        currentUpdateId = commentId;
        focus('comments'+gallery_id);
    };
};


angular.module("wedApp")
    .controller("ImagesListCtrl", ["$http", function ($http) {
        var self = this;
        $http.get("/api/galleries")
            .success(function (images) {
                self.images = images;
            });

        self.like = function (gallery) {
            $http.get("/api/gallery/" + gallery.id + "/like")
                .success(function () {
                    if (gallery.likeCount) {
                        gallery.likeCount++;
                    } else {
                        gallery.likeCount = 1;
                    }
                });
        };
    }])


    .controller("ImageUploadCtrl", ["$http", function ($http) {
        var self = this;
        var fileToUpload;
        var uploadApi = "/api/gallery";

        self.uploadFile = function (files) {
            fileToUpload = files[0];
        };
        self.uploading = false;

        self.upload = function () {
            var fd = new FormData();
            fd.append("photo", fileToUpload);
            self.uploading = true;
            $http.post(uploadApi, fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            }).success(function () {
                self.uploading = false;
                self.showUpload = false;
            }).error(function () {
                self.uploading = false;
                alert("Some Error Occurred. Please try again.")
            });
        };

    }]);

function createComment() {
    return ({id: '', gallery_id: '', comments: '', comment_id: '1', attendees_id: '1'});
}

function fillupComment(comment, gallery_id) {
    comment.gallery_id = gallery_id;
    return ({id: comment.id, gallery_id: gallery_id, comments: comment.comments,
        comment_id: comment.comment_id, attendees_id: comment.attendees_id, updatedDatetime: new Date});
}

function focus($timeout, $window) {
    return function(id) {
        $timeout(function() {
            var element = $window.document.getElementById(id);
            if(element)
                element.focus();
        });
    };
}

